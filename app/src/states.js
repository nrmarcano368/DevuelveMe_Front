angular.module('DevuelveMeApp')

.config(function ($stateProvider, $urlRouterProvider) {

  $urlRouterProvider.otherwise('welcome');
  $stateProvider

  //MAIN STATE
  .state('main', {
    url: '/',
    templateUrl: 'views/main.html',
    controller: 'mainCtrl'       
  })

  //WELCOME STATE
  .state('main.welcome', {
    url: 'welcome',
    templateUrl: 'views/welcome.html'       
  })            

  //BOOK STATE
  .state('main.books', {
    url: 'books',
    templateUrl: 'views/books.html', 
    controller: 'bookCtrl',
    resolve: {
      bookList: function(bookResource) {
        return bookResource.query().$promise;
      }
    }
  })

  //FRIEND STATE
  .state('main.friends', {
    url: 'friends',
    templateUrl: 'views/friends.html', 
    controller: 'friendCtrl',
    resolve: {
      friendList: function(friendResource) {
        return friendResource.query().$promise;
      }
    }      
  })

  //LOANS STATE
  .state('main.loans', {
    url: 'loans',
    templateUrl: 'views/loans.html',
    controller: 'loanCtrl',
    resolve: {
      loanList: function(loanResource) {
        return loanResource.query().$promise;
      },
      friendList: function(friendResource) {
        return friendResource.query().$promise;
      }, 
      bookList: function(bookResource) {
        return bookResource.query().$promise;
      }               
    }                         
  })

  //STATISTICS STATE
  .state('main.statistics', {
    url: 'statistics',
    templateUrl: 'views/statistics.html',       
    controller: 'statisticsCtrl',
    resolve: {
      bookList: function(bookResource) {
        return bookResource.query().$promise;
      },
      loanList: function(loanResource) {
        return loanResource.query().$promise;
      },
      friendList: function(friendResource) {
        return friendResource.query().$promise;
      }
    }    
  })
  // .state('main.statistics.notAvailable', {
  //   url: '',
  //   templateUrl: 'views/friendsLoans.html',       
  //   controller: 'statisticsCtrl',
  // }) 
});