angular.module('DevuelveMeApp')

.factory('loanResource', function ($resource) {
  var res = $resource('http://localhost:3000/loans/:id', 
  {
    id: '@id'
  }, 
  {
    update: {
      method: 'PUT'
    }
  });
  return res;
});