angular.module('DevuelveMeApp')

.factory('bookResource', function ($resource) {
  var res = $resource('http://localhost:3000/books/:id', 
  {
    id: '@id'
  }, 
  {
    update: {
      method: 'PUT'
    }
  });
  return res;
});