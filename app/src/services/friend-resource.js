angular.module('DevuelveMeApp')

.factory('friendResource', function ($resource) {
  var res = $resource('http://localhost:3000/friends/:id', 
  {
    id: '@id'
  }, 
  {
    update: {
      method: 'PUT'
    }
  });
  return res;
});