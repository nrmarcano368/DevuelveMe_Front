angular.module('DevuelveMeApp')
.controller('bookCtrl', function ($scope, bookResource, bookList) {

  $scope.booksArray = bookList;

  $scope.createBook = function() {
    book = new bookResource({
      title: $scope.title,
      author: $scope.author,
      isbn: $scope.isbn
    });
    book.$save(function() {
      alert("Libro registrado exitosamente.");
      $scope.title = "";
      $scope.author = "";
      $scope.isbn = "";
      $scope.booksArray.push(book);
    }, function(response) {
      alert(response.data.errors);
    });
  };

});