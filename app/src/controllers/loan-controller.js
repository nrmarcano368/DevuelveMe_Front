angular.module('DevuelveMeApp')
.controller('loanCtrl', function ($scope, loanResource, loanList, bookList, friendList, bookResource) {

  $scope.loansArray = [];
  $scope.friendsArray = friendList;
  $scope.booksArray = [];

  loanList.forEach(function(loan){
    if (loan.status == "Por devolver") {
      $scope.loansArray.push(loan);
    };
  });

  bookList.forEach(function(book){
    if (book.status == "Disponible") {
      $scope.booksArray.push(book);
    }    
  });

  $scope.return = function(index ,loan) {
    loan.$update(function() {
      $scope.loansArray.splice(index, 1);
      alert("Libro devuelto exitosamente.");
    },function(response) {
      alert(response.data.errors);
    });
  }; 

  $scope.createLoan = function() {
    if ($scope.friendSelected && $scope.bookSelected){
      loan = new loanResource({
        friend_id: $scope.friendSelected.id,
        book_id: $scope.bookSelected.id
      });
      loan.$save(function() {
        alert("Prestamo realizado exitosamente.");
        $scope.friendSelected = "";
        $scope.bookSelected = "";
      });
    } else if ($scope.friendSelected) {
      alert("Por favor, selecciona un libro");
    } else if ($scope.bookSelected) {
      alert("Por favor, selecciona un amigo");
    } else {
      alert("Por favor, selecciona un amigo y un libro");
    };
  };

});