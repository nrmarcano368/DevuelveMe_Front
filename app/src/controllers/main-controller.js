angular.module('DevuelveMeApp')
.controller('mainCtrl', function ($scope, $state) {

  $scope.goTo = function(state) {
    $state.go(state)
  };
  
});