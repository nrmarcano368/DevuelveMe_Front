angular.module('DevuelveMeApp')

.controller('statisticsCtrl', function ($scope, $state, bookList, loanList, friendList) {
  $scope.friendList = friendList;
  $scope.loanList = loanList;
  $scope.borrowedArray = [];
  $scope.availableArray = [];
  $scope.loanArray = [];
  $scope.borrowedFriendArray = [];
  $scope.availableFriendArray = [];
  $scope.borrowedFriendq = 0;
  $scope.availableFriendq = 0;

  bookList.forEach(function(book) {
    if (book.status == "Prestado") {
      $scope.borrowedArray.push(book);
    } else {
      $scope.availableArray.push(book);
    }
  });

  loanList.forEach(function (loan) {
    if (loan.status == "Por devolver") {
      $scope.loanArray.push(loan);
    };
  });

  $scope.availableArray.forEach(function (book) {
    temp = {book: book.title,
            status: 'Devuelto',
            name: ''};
    $scope.loanArray.push(temp);
  });

  $scope.borrowedq = $scope.borrowedArray.length;
  $scope.availableq = $scope.availableArray.length;

  $scope.labels = ["Disponibles", "Prestados"];
  $scope.data = [$scope.availableq, $scope.borrowedq];
  $scope.colours = [{fillColor:'#46BFBD',
                     strokeColor: '#46BFBD',
                     highlightFill: '#46BFBD',
                     highlightStroke: '#46BFBD'
                    }, 
                    {fillColor:'#DCDCDC',
                     strokeColor: '#DCDCDC',
                     highlightFill: '#DCDCDC',
                     highlightStroke: '#DCDCDC'
                    }];

  $scope.labelsFriend = ["Devueltos", "Por devolver"];

  $scope.drawGraph = function(){
    $scope.borrowedFriendArray = [];
    $scope.availableFriendArray = [];
    $scope.borrowedFriendq = 0;
    $scope.availableFriendq = 0;

    if ($scope.friendSelected){
      $scope.loanList.forEach(function(book) {
        if (book.friend_id == $scope.friendSelected.id){
          if (book.status == "Por devolver") {
            $scope.borrowedFriendArray.push(book);
          } else {
            $scope.availableFriendArray.push(book);
          };
          $scope.borrowedFriendq = $scope.borrowedFriendArray.length;
          $scope.availableFriendq = $scope.availableFriendArray.length;
        };
      });
    };

    $scope.dataFriend = [$scope.availableFriendq, $scope.borrowedFriendq];
  };

});