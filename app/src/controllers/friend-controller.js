angular.module('DevuelveMeApp')
.controller('friendCtrl', function ($scope, friendResource, friendList) {

  $scope.friendsArray = friendList;

  $scope.createFriend = function() {
    friend = new friendResource({
      name: $scope.name,
      last_name: $scope.last_name,
      phone: $scope.phone,
      email: $scope.email
    });
    friend.$save(function() {
      alert("Amigo registrado exitosamente.");
      $scope.name = "";
      $scope.last_name = "";
      $scope.phone = "";
      $scope.email = "";
      $scope.friendsArray.push(friend);
    }, function(response) {
      alert(response.data.errors);       
    });
  };

});